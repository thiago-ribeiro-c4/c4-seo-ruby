
# encoding: utf-8

module UI
    module Pages

      class MoreInspirationPage < SitePrism::Page
           set_url '/dicas/mais-inspiracao/pra-se-cuidar'

      end
  end
end
