# encoding: utf-8

module UI
    module Pages

      class ChefRecipesPage < SitePrism::Page
           set_url '/dicas/chef-carrefour'
           element :ldJson3, :xpath, "(//script[@type='application/ld+json'])[3]", visible: false


           def encontrar_ldJson
             return JSON.parse(ldJson3.text(:all))
           end
      end
  end
end
