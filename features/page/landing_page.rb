# encoding: utf-8

module UI
    module Pages

      class LandingPage < SitePrism::Page
           set_url '/notebook?destaque=5150973&isGrid=false'
           element :divSchemaProduct, "div[itemtype='https://schema.org/Product']"
      end
  end
end
