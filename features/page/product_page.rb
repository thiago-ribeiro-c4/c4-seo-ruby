# encoding: utf-8

module UI
    module Pages

      class ProductPage < SitePrism::Page
          element :buyProductButton, '#buyProductButton'
          element :schemaProduct, "div[itemtype='https://schema.org/Product']"
      end
  end
end
