# encoding: utf-8

module UI
    module Pages

      class SearchPage < SitePrism::Page
          element :termoBuscado, 'body > main > div > div.container-fluid > div.container-fluid > div > div.col-xs-12.col-md-4.col-lg-5.searchNavigation__infoResult > p > strong'
          element :itemList, '[itemprop="itemOffered"]'
        
      end
  end
end
