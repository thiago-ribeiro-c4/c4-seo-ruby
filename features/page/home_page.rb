# encoding: utf-8

module UI
    module Pages

      class HomePage < SitePrism::Page
          include RSpec::Matchers
             set_url '/'
             element :head, :xpath, "/html/head", visible: false
             element :footer, :xpath, "//footer", match: :first
             element :ldJson1, :xpath, "//script[@type='application/ld+json']", visible: false, match: :first
             element :ldJson2, :xpath, "(//script[@type='application/ld+json'])[2]", visible: false
             element :html, :xpath, "/html", visible: false
             element :fontcrf, :xpath, "//style[@type='text/css']", visible: false, match: :first
             element :title, :xpath, '/html/head/title', visible:false
             element :firstCarrosel, "div[class^='carousel']", match: :first
             element :firstProdutct, "div[class^='owl-item']", match: :first
             element :searchBox, '#js-site-search-input'
             element :btnSearch, '#searchForm > button'

          def carrosel
            return "div[class^='carousel']"
          end
          def divProdutoVitrine
            return "div[class^='owl-item']"
          end

          def encontrar_tags_head(busca,resultados)
            expect(
              (head.all busca, visible: false).count
            ).to eq resultados
          end

          def encontrar_tags_html(busca,resultados)
            expect(
              (html.all busca, visible: false).count
            ).to eq resultados
          end

          def encontrar_tags_footer(busca,resultados)
            expect(
              (footer.all busca).count
            ).to eq resultados
          end

          def encontrar_ldJson(chave)
            return JSON.parse(ldJson1.text(:all))[chave]
          end

          def encontrar_ldJsonWebSite
            return JSON.parse(ldJson2.text(:all))
          end

          def validar_tag_html_nao_inserida(busca)
            expect(
              (html.all(:xpath, busca, visible: false)).count
            ).to eq 0
          end
          def validar_tag_html_quantidade(busca,quantidade)
            expect(
              (html.all(:xpath, busca, visible: false)).count
            ).to eq quantidade
          end

          def validar_pagina_correta(contem)
            url = URI.parse(current_url).to_s
            if ! url.include? contem
              fail("Pagina Inválida: " + url)
            end
          end
      end

    end
end
