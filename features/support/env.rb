require 'rspec'
require 'page-object'
require 'data_magic'
require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'site_prism'
require 'yaml'
require 'capybara/poltergeist'
require 'pry'

World(PageObject::PageFactory)

BROWSER = ENV["BROWSER"]
ENVIRONMENT_TYPE = ENV['ENVIRONMENT_TYPE']

## variable which loads the data file according to the environment
CONFIG = YAML.load_file(File.dirname(__FILE__) + "/config/#{ENVIRONMENT_TYPE}.yaml")

Capybara.register_driver :selenium do |app|
  if BROWSER.eql?('chrome')
    Capybara::Selenium::Driver.new(app,
    :browser => :chrome,
    :desired_capabilities => Selenium::WebDriver::Remote::Capabilities.chrome(
      'chromeOptions' => {
        'args' => [ "--start-maximized" ]
      }
    )
  )
  elsif BROWSER.eql?('firefox')
    Capybara::Selenium::Driver.new(app, :browser => :firefox, :marionette => true)
  elsif BROWSER.eql?('internet_explorer')
    Capybara::Selenium::Driver.new(app, :browser => :internet_explorer)
  elsif BROWSER.eql?('safari')
    Capybara::Selenium::Driver.new(app, :browser => :safari)
  elsif BROWSER.eql?('poltergeist')
    capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
     'chromeOptions': {
       'args': %w[ no-sandbox --ignore-certificate-errors headless disable-popup-blocking disable-gpu window-size=1366,1024 --enable-logging=v=1  ]
     }
   )

   Capybara::Selenium::Driver.new(app, browser: :chrome, desired_capabilities: capabilities)
  end
end

## set default max wait and maximize browser
Capybara.default_max_wait_time = 30
