# encoding: utf-8
# !/usr/bin/env ruby

Dir[File.join(File.dirname(__FILE__), '../pages/*.rb')].each { |file| require file }

module UI
  module Pages
    def home_page
      UI::Pages::HomePage.new
    end
    def product_page
      UI::Pages::ProductPage.new
    end
    def search_page
      UI::Pages::SearchPage.new
    end
    def landing_page
        UI::Pages::LandingPage.new
    end
    def tree_result_page
      UI::Pages::TreeResultPage.new
    end
    def chef_recipes_page
      UI::Pages::ChefRecipesPage.new
    end
    def more_inspiration_page
      UI::Pages::MoreInspirationPage.new
    end
  end
end
