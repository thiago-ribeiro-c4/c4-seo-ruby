#language: pt
@seo-homepage
Funcionalidade:  Validar Home Page
    Como um usuário
    Quero acessar a home page Carrefour
    De modo a validar as metatag para otimização SEO

Contexto:
  Dado que eu acesse a homepage do Carrefour


  Esquema do Cenário: Validar metatags com quantidades definidas
      Então eu vizualizo a metatag tipo "<tipo>" valor "<metatag>" com <resultados> resultados
  Exemplos:
  | tipo      | metatag   | resultados  |
  | name      | robots    |     1       |
  | name      | keywords  |     0       |
  | charset   | utf-8     |     1       |


  Esquema do Cenário: Validar metatag com tipo com content e quantidades definidas
    Então eu vizualizo a metatag tipo "<tipo>" valor "<metatag>" com "<content>" com <resultados> resultados
  Exemplos:
    | tipo        | metatag          |                           content                                                    | resultados |
    | name        | viewport         | width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1                |    1       |
    | http-equiv  | Content-Type     | text/html; charset=utf-8                                                             |    1       |
    | http-equiv  | X-UA-Compatible  | IE=edge                                                                              |    1       |
    | name        | twitter:card     |  summary                                                                             |    1       |
    | name        | twitter:image    |  /_ui/responsive/theme-carrefour/images/logo_carrefourgif.gif                        |    1       |
    | name        | twitter:site     | @carrefourbrasil                                                                     |    1       |
    | property    | og:type          | business.business                                                                    |    1       |
    | property    | og:image         |  /_ui/responsive/theme-carrefour/images/logo_carrefourgif.gif                        |    1       |
    | property    | og:site_name     |  Carrefour                                                                           |    1       |


  Cenário: Validar metatag description não podendo estar vazia
      Então eu vizualizo a metatag description não podendo estar vazia


  Esquema do Cenário: Validar link rel canonical não vazio
      Então eu vizualizo a tag "<tag>" tipo "<tipo>" valor "<valor>" e "<atributo>" não vazio com apenas uma referencia
  Exemplos:
  | tag      | tipo       | valor               | atributo  |
  | link     | rel        | canonical           | href      |
  | meta     | name       | twitter:url         | content   |
  | meta     | name       | twitter:title       | content   |
  | meta     | name       | twitter:description | content   |
  | meta     | property   | og:url              | content   |
  | meta     | property   | og:title            | content   |
  | meta     | property   | og:description      | content   |


  Cenário: Validar itemprop telefone no footer
    Então eu verifico o itemprop telephone e com telefone preenchido


  Cenário: Validar itemprop sameAs
      Então eu valido o itemProp sameAs do facebook,twitter, instagram e youtube


  Cenário: Validar schema PostalAdress
      Então eu valido o schema PostalAdress


  Cenário: Validar schema WebSite
        Então eu valido o schema WebSite


  Esquema do Cenário: Validar a não existência de tags link
      Então valido que não existe nenhuma referencia do link href "<href>"
    Exemplos:
    |             href              |
    | carrefourasmcheckoutaddon.css |
    | assistedservicestorefront.css |
    | sapprodrecoaddon.css          |


  Esquema do Cenário: Validar a existência ou não de tags script
        Então valido que existe <quantidade> referencia do script src "<src>"
      Exemplos:
      |             src        | quantidade |
      | clipboard.min.js       | 0          |
      | sapprodrecoaddon.js    | 0          |
      | generatedVariables.js  | 1          |
      | pinit                  | 0          |


  Cenário: Validar a não existencia da tag href='javascript:;'
    Então valido que a tag "href='javascript:;'" não há nenhuma referencia

  @todo
  Esquema do Cenário: Validar a existência ou não de tags script com atributo async
    Então valido que não existe <quantidade> referencia do script src "<src>" com atributo async
  Exemplos:
  |             src        | quantidade |
  | analyticsmediator.js   | 1          |


  Esquema do Cenário: Validar DNS PREFETCHING
    Então eu verifico que o href "<href>" do dns prefetch tenha <quantidades> resultados
  Exemplos:
  |                 href                | quantidades |
  | static-css.preprod.carrefour.com.br |      0      |
  | static-js.preprod.carrefour.com.br  |      0      |
  | assets.adobedtm.com                 |      1      |
  | dpm.demdex.net                      |      1      |
  | bbcdn.githack.com                   |      1      |
  | accounts.google.com                 |      1      |
  | adobedemoamericas94.tt.omtrdc.net   |      1      |
  | api.soclminer.com.br                |      1      |
  | api.zanox.com                       |      1      |
  | apis.google.com                     |      1      |
  | ash.creativecdn.com                 |      1      |
  | carrefourbr.demdex.net              |      1      |
  | carrefourbr.sc.omtrdc.net           |      1      |
  | cdn.appdynamics.com                 |      1      |
  | ck.solocpm.com                      |      1      |
  | col.eum-appdynamics.com             |      1      |
  | connect.facebook.net                |      1      |
  | googleads.g.doubleclick.net         |      1      |
  | match.adsrvr.org                    |      1      |
  | plugins.soclminer.com.br            |      1      |
  | pubads.g.doubleclick.net            |      1      |
  | rm.estadao.com.br                   |      1      |
  | script.crazyegg.com                 |      1      |
  | dis.criteo.com                      |      1      |
  | secureaud.solocpm.com               |      1      |
  | ssl.gstatic.com                     |      1      |
  | static.zanox.com                    |      1      |
  | staticxx.facebook.com               |      1      |
  | tracker.pensebig.com.br             |      1      |
  | us.creativecdn.com                  |      1      |
  | www.facebook.com                    |      1      |
  | www.google.com                      |      1      |
  | www.googleadservices.com            |      1      |
  | www5.smartadserver.com              |      1      |
  | fonts.gstatic.com                   |      0      |
  | landing.carrefourdigital.com.br     |      0      |
  | static.carrefourdigital.com.br      |      0      |


  Esquema do Cenário: Validar DNS PREFETCHING por ambiente
    Então eu verifico que o href "<href>" por ambiente do dns prefetch tenha <quantidades> resultados
    Exemplos:
    |  href              | quantidades |
    | static-css         |      1      |
    | static-js          |      1      |
    | static             |      1      |


  Esquema do Cenário: Validar DNS preconnect
    Então eu verifico que o href "<href>" do dns preconnect tenha 1 resultados
  Exemplos:
  |                 href                |
  | accounts.google.com                 |
  | adobedemoamericas94.tt.omtrdc.net   |
  | api.soclminer.com.br                |
  | api.zanox.com                       |
  | apis.google.com                     |
  | ash.creativecdn.com                 |
  | carrefourbr.demdex.net              |
  | carrefourbr.sc.omtrdc.net           |
  | cdn.appdynamics.com                 |
  | ck.solocpm.com                      |
  | col.eum-appdynamics.com             |
  | connect.facebook.net                |
  | googleads.g.doubleclick.net         |
  | match.adsrvr.org                    |
  | plugins.soclminer.com.br            |
  | pubads.g.doubleclick.net            |
  | rm.estadao.com.br                   |
  | script.crazyegg.com                 |
  | dis.criteo.com                      |
  | secureaud.solocpm.com               |
  | ssl.gstatic.com                     |
  | static.zanox.com                    |
  | staticxx.facebook.com               |
  | tracker.pensebig.com.br             |
  | us.creativecdn.com                  |
  | www.facebook.com                    |
  | www.google.com                      |
  | www.googleadservices.com            |
  | www5.smartadserver.com              |


  Cenário: Validar script de font crficon
    Então eu valido o style crficon


  Cenário: Validar título da pagina
    Então eu valido a tag title da pagina não vazia


  Esquema do Cenário: Validar schema de produtos nas vitrines
    Quando eu carregar todas as vitrines da home_page
    Então eu valido o schema "<schema>"

    Exemplos:
    | schema                                                                                  |
    | div[itemtype='https://schema.org/Product']                                              |
    | meta[itemprop='name']:not([content=''])                                                 |
    | meta[itemprop='url']:not([content=''])                                                  |
    | meta[itemprop='image']:not([content='']):not([alt='']):not([title=''])[content*='.jpg'] |
    | [itemprop='offers'][itemtype='https://schema.org/Offer']                                |



  Esquema do Cenário: Validar schema de prodotos nas vitrines com itens no estoque
  Quando eu carregar todas as vitrines da home_page
  Então eu valido o schema "<schemaEmEstoque>" e "<schemaSemEstoque>"

  Exemplos:
    |                    schemaEmEstoque                              |                   schemaSemEstoque                              |
    | [itemprop='availability'][href='https://schema.org/InStock']    | [itemprop='availability'][href='https://schema.org/OutOfStock'] |
    | [itemprop='price']:not([content=''])                            | [itemprop='availability'][href='https://schema.org/OutOfStock'] |
    | [itemprop='priceCurrency'][content='BRL']                       | [itemprop='availability'][href='https://schema.org/OutOfStock'] |
    | [class='prd-price-installments'][itemprop='itemCondition']      | [itemprop='availability'][href='https://schema.org/OutOfStock'] |
