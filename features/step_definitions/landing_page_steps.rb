Dado("que eu esteja na landing Page") do
  home_page.load
  landing_page.load
end

Então("eu valido que o schema product tem apenas uma referencia na pagina") do
  expect(
    landing_page.divSchemaProduct != nil
).to be_truthy
end
