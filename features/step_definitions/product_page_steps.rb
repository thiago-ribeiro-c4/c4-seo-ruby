# encoding: utf-8
Quando("eu clicar em um produto") do

  home_page.wait_for_firstProdutct

  firstProduct = nil

  loop do
    firstProduct = home_page.firstProdutct

    href = firstProduct.find("a", match: :first)[:href]
    break if href.include? "/p/"
  end

  firstProduct.find("img", match: :first).click

  product_page.wait_for_buyProductButton

  home_page.validar_pagina_correta("/p/")

end

Então("eu valido o itemProp sameAs do facebook,twitter, instagram e youtube da pdp") do
  valores = home_page.encontrar_ldJsonWebSite['sameAs']
  comparar = ['https://www.facebook.com/CarrefourBR/','https://twitter.com/carrefourbrasil','https://www.youtube.com/user/carrefourbrasil',
    'https://instagram.com/carrefourbrasil/']
  erro = false
  logErro = ''
   #comparar.zip(valores).map {|a,b| a == b ?  (puts b) : (puts(a))  }
   comparar.zip(valores).map {|a,b| a == b ?  "" : (erro = true, logErro = 'Esperado '+ a +' Obitido '+ b)  }

   if erro
     fail(logErro)
   end
end

Então("eu valido o schema PostalAdress da pdp") do
  expect(home_page.encontrar_ldJsonWebSite['address']["@type"]).to eq "PostalAddress"
end

Então("eu valido o schema WebSite da pdp não tenha nenhum registro") do
  expect(valoresObtidos = home_page.encontrar_ldJson("WebSite")).to eq nil
end

Então("eu valido o schema BreadcrumbList") do
  expect(home_page.encontrar_ldJson("@type")).to eq "BreadcrumbList"
end

Então("eu valido o style crficon da pdp") do
  expect(
    home_page.fontcrf.text(:all)
  ).to eq  "@font-face{font-family:'crficon';src:url('/_ui/responsive/theme-carrefour/fonts/crf-icons/crficon.eot?8h7jv0');src:url('/_ui/responsive/theme-carrefour/fonts/crf-icons/crficon.eot?8h7jv0#iefix') format('embedded-opentype'),url('/_ui/responsive/theme-carrefour/fonts/crf-icons/crficon.ttf?8h7jv0') format('truetype'),url('/_ui/responsive/theme-carrefour/fonts/crf-icons/crficon.woff?8h7jv0') format('woff'),url('/_ui/responsive/theme-carrefour/fonts/crf-icons/crficon.svg?8h7jv0#crficon') format('svg');font-weight:normal;font-style:normal} @font-face{font-family:'Glyphicons Halflings';src:url('/_ui/responsive/theme-carrefour/fonts/glyphicons-halflings-regular.eot');src:url('/_ui/responsive/theme-carrefour/fonts/glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'),url('/_ui/responsive/theme-carrefour/fonts/glyphicons-halflings-regular.woff') format('woff'),url('/_ui/responsive/theme-carrefour/fonts/glyphicons-halflings-regular.ttf') format('truetype'),url('/_ui/responsive/theme-carrefour/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg')} .glyphicon{position:relative;top:1px;display:inline-block;font-family:'Glyphicons Halflings';font-style:normal;font-weight:normal;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}"

end

Então("eu valido o schema produto com a tag {string}") do |tag|
   expect(
           product_page.schemaProduct.all(tag, visible: false).count
       ).to eq 1
end


Quando("eu obter a url da imagem no schema BreadcrumbList") do
  @urlImage = home_page.encontrar_ldJson("itemListElement")[1]["item"]["image"]
end

Então("eu valido a tag {string} com valor {string} com a url obtida com apenas um resultado") do |tipo, name|
    home_page.encontrar_tags_head "meta[#{tipo}='#{name}'][content='#{@urlImage}']", 1
end
