Quando("digitar o termo {string} no campo busca") do |busca|
  home_page.searchBox.set busca
  home_page.btnSearch.click
  search_page.wait_for_termoBuscado
  home_page.validar_pagina_correta("/busca/")
  expect(search_page.termoBuscado.text().include? busca).to be_truthy

  Capybara.execute_script("window.scrollBy(0,2500)")
  sleep 5
end

Então("eu valido o schema itemList") do
  expect(
    search_page.itemList != nil
).to be_truthy

end

Então("eu valido a tag {string} com atributo {string} e valor {string} com a mesmo numero de referencias que a quantidade de produtos na lista") do |tag, atributo, valor|
  expect(
    search_page.itemList.all("#{tag}[#{atributo}='#{valor}']", visible: false).count
  ).to eq search_page.itemList.all('div[itemprop="itemListElement"]').count
end

Então("eu valido a tag meta itemprop url com url de pdp válida com a mesmo numero de referencias que a quantidade de produtos na lista") do
  expect(
    search_page.itemList.all(:xpath, "//meta[@itemprop='url'][contains(@content, '/p/')]", visible: false).count
  ).to eq search_page.itemList.all('div[itemprop="itemListElement"]').count
end

Então("eu valido a tag meta itemprop image extenção do arquivo seja jpg e alt e title preenchido com o title da imagem") do
  expect(
    search_page.itemList.all("img[itemprop='image']:not([src='']):not([alt='']):not([title=''])[src*='.jpg']", visible: false).count
  ).to eq search_page.itemList.all('div[itemprop="itemListElement"]').count
end
