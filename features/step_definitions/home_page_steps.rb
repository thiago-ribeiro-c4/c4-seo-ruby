Dado("que eu acesse a homepage do Carrefour") do
  home_page.load
end

Então("eu vizualizo a metatag tipo {string} valor {string} com {int} resultados") do |type, name, count|
  home_page.encontrar_tags_head "meta[#{type}='#{name}']", count
end

Então("eu vizualizo a metatag tipo {string} valor {string} com {string} com {int} resultados") do |type, name, content, count|
  home_page.encontrar_tags_html "meta[#{type}='#{name}'][content='#{content}']", count
end

Então("eu vizualizo a metatag description não podendo estar vazia") do
  home_page.encontrar_tags_head "meta[name='description']:not([content=''])", 1
end

Então("eu vizualizo a tag {string} tipo {string} valor {string} e {string} não vazio com apenas uma referencia") do |tag, type, value, attribute|
  home_page.encontrar_tags_head "#{tag}[#{type}='#{value}']:not([#{attribute}=''])", 1
end

Então("eu verifico o itemprop telephone e com telefone preenchido") do
  home_page.encontrar_tags_footer "span[itemprop=telephone]" , 1
  expect(home_page.footer.find("span[itemprop=telephone]").text).to eq "3003-2288"
end

Então("eu valido o itemProp sameAs do facebook,twitter, instagram e youtube") do
  valores = home_page.encontrar_ldJson('sameAs')
  comparar = ['https://www.facebook.com/CarrefourBR/','https://twitter.com/carrefourbrasil','https://www.youtube.com/user/carrefourbrasil',
    'https://instagram.com/carrefourbrasil/']
  erro = false
  logErro = ''
   comparar.zip(valores).map {|a,b| a == b ?  "" : (erro = true, logErro = 'Esperado '+ a +' Obitido '+ b)  }

   if erro
     fail(logErro)
   end
end

Então("eu valido o schema PostalAdress") do
    expect(home_page.encontrar_ldJson('address')["@type"]).to eq "PostalAddress"
end

Então("eu valido o schema WebSite") do
  valorAComparar = JSON.parse('{"@context": "https://schema.org","@type": "WebSite","url": "https://www.carrefour.com.br","potentialAction": {"@type": "SearchAction","target": "https://www.carrefour.com.br/busca/?termo={search_term_string}","query-input": "required name=search_term_string"}}')
  valoresObtidos = home_page.encontrar_ldJsonWebSite

  erro = false
  logErro = ''

  valorAComparar.zip(valoresObtidos).map{|a,b| a == b ?  "" : (logErro = a, erro=true)   }

  if erro
    fail(logErro.to_s)
  end

end

Então("valido que não existe nenhuma referencia do link href {string}") do |href|
  home_page.validar_tag_html_nao_inserida("//link[@href='#{href}']")
end

Então("valido que existe {int} referencia do script src {string}") do |qtd, src|
  home_page.validar_tag_html_quantidade("//script[contains(@src, '#{src}')]",qtd)
end

Então("valido que a tag {string} não há nenhuma referencia") do |tag|
  home_page.validar_tag_html_nao_inserida("//*[#{tag}]")
end

Então("valido que não existe {int} referencia do script src {string} com atributo async") do |qtd, src|
  home_page.validar_tag_html_quantidade("//script[contains(@src, '#{src}')]",qtd)
end

Então("eu verifico que o href {string} do dns prefetch tenha {int} resultados") do |href, qtd|
    home_page.validar_tag_html_quantidade("//link[@rel='dns-prefetch'][contains(@href, '#{href}')]",qtd)
end

Então("eu verifico que o href {string} por ambiente do dns prefetch tenha {int} resultados") do |href, qtd|
  step "eu verifico que o href '" + CONFIG["#{href}"] +"' do dns prefetch tenha #{qtd} resultados"
end

Então("eu verifico que o href {string} do dns preconnect tenha {int} resultados") do |href, qtd|
  home_page.validar_tag_html_quantidade("//link[@rel='preconnect'][contains(@href, '#{href}')][@crossorigin]",qtd)
end

Então("eu valido o style crficon") do
  expect(
    home_page.fontcrf.text(:all)
  ).to eq  CONFIG['crficon']

end

Então("eu valido a tag title da pagina não vazia") do
  expect(home_page.title.text(:all).length > 0).to be_truthy
end

Quando("eu carregar todas as vitrines da home_page") do
  home_page.wait_for_firstCarrosel
  @list = all(home_page.carrosel)
end

Então("eu valido o schema {string}") do |cssSeletor|
  @list.each do |vitrine|
      home_page.wait_for_firstProdutct
      produtos = vitrine.all(home_page.divProdutoVitrine, visible: false)
      expect(produtos.length > 0).to be_truthy
      produtos.each do |produto|
      #puts produto.find(".prd-name", visible: false).text(:all)
      expect(
              produto.all(cssSeletor, visible: false).count
          ).to eq 1
      end
  end
end

Então("eu valido o schema {string} e {string}") do |inStock, outOfStock|
  @list.each do |vitrine|
      home_page.wait_for_firstProdutct
      produtos = vitrine.all(home_page.divProdutoVitrine, visible: false)
      expect(produtos.length > 0).to be_truthy
      produtos.each do |produto|
      #puts produto.find(".prd-name", visible: false).text(:all)
      cssSeletor = inStock
      begin
        produto.find(inStock, visible: false)
      rescue
        cssSeletor = outOfStock
      ensure
          expect(
                produto.all(cssSeletor, visible: false).count
              ).to eq 1
      end
      end
  end
end
