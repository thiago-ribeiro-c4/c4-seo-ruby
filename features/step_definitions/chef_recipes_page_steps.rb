Dado("que eu acesse a chef recipes page") do
  chef_recipes_page.load
end

Então("eu valido o itemProp sameAs do facebook,twitter, instagram e youtube da chief") do
 valores = chef_recipes_page.encontrar_ldJson["sameAs"]

 comparar = ['https://www.facebook.com/CarrefourBR/','https://twitter.com/carrefourbrasil','https://www.youtube.com/user/carrefourbrasil',
   'https://instagram.com/carrefourbrasil/']
 erro = false
 logErro = ''
  comparar.zip(valores).map {|a,b| a == b ?  "" : (erro = true, logErro = 'Esperado '+ a +' Obitido '+ b)  }

  if erro
    fail(logErro)
  end
end
